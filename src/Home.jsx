import React,{useState,useEffect} from 'react';
import './srcCSS/universal.css';
import {Carousel, Card, Button, Row, Col, Container, Image } from 'react-bootstrap';
import "react-responsive-carousel/lib/styles/carousel.min.css"
import HomeRandom from './components/HomeRandom'




export default function Home() {

	const [prodDetails,setProductDetails] = useState([])

	useEffect(()=>{
		fetch(`http://localhost:4000/products/retrieveAllActive`)
		.then(res => res.json())
		.then(data => {
			localStorage.getItem('token',data.accessToken);
			setProductDetails(data.map(product => {
				return (
					<HomeRandom prodProp={product}/>
				)
			}))	
		})
		.catch(err =>{
			console.log(err)
			alert('error')
		})
	},[])

	let rand = prodDetails[Math.floor(Math.random(3)*prodDetails.length)]
	let rand1 = prodDetails[Math.floor(Math.random(2)*prodDetails.length)]

	return(
		<Container fluid>
			<Carousel>  
			  <Carousel.Item>
			    <img
			      src="https://drive.google.com/uc?export=view&id=1MPsW5HyC1Fj0d_gOlW35HzZ2xFtRH18U"
			      alt="First slide" 
			      fluid 
			    />
			  </Carousel.Item>			  
			  <Carousel.Item>
			    <img 
			      src="https://cdn.wallpapersafari.com/94/77/J7zOMp.jpg"
			      alt="First slide"
			      fluid 
			    />
			  </Carousel.Item>
			  <Carousel.Item>
			    <img
			      src="https://wallpaper-mania.com/wp-content/uploads/2018/09/High_resolution_wallpaper_background_ID_77700168700.jpg"
			      alt="Second slide" 
			      fluid 
			    />
			  </Carousel.Item>
			  <Carousel.Item>
			    <img
			      src="https://cdn.wallpapersafari.com/35/51/1H9Q6m.jpg"
			      alt="Third slide" 
			      fluid 
			    />
			  </Carousel.Item>
			</Carousel>
			<h1 className='text-center'>Featured Products</h1>
			<Container>
			<div style={{float:"left"}}>
			{rand}
			</div>
			<div style={{float:"right"}}>
			{rand1}
			</div>
			</Container>
		</Container>

	)
}
import {Form, Button} from 'react-bootstrap';
import React,{useState,useEffect,useContext} from 'react';
import {Redirect,useHistory} from 'react-router-dom';
import UserContext from '../userContext';
import Swal from 'sweetalert2';

export default function Register(){

	const {user} = useContext(UserContext);

	const history = useHistory();

	const [fName, setFName] = useState("")
	const [lName, setLName] = useState("")
	const [email, setEmail] = useState("")
	const [mobileNo, setMobileNo] = useState("")
	const [password, setPassword] = useState("")
	const [isActive,setIsActive] = useState(false)

	useEffect(()=>{

		if((fName !== "" && lName !== "" && email !== "" && mobileNo !== "" && password !=="") && (mobileNo.length === 11)){

			setIsActive(true)

		} else {

			setIsActive(false)

		}

	},[fName,lName,email,mobileNo,password])

	function registerUser(e){

		e.preventDefault()

		fetch('http://localhost:4000/users/register',{
			method: 'POST',
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				fName: fName,
				lName: lName,
				email: email,
				mobileNo: mobileNo,
				password: password,
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data.email){
				Swal.fire({
					icon: "success",
					title: "Registration Successful!",
					text: `Thank you for registering ${data.email}`
				})
				setFName("")
				setLName("")
				setEmail("")
				setMobileNo("")
				setPassword("")
				history.push('/login')

			}
			else{
				Swal.fire({
					icon: "error",
					title: "Registration Failed",
					text: data.message
				})
			}
		})

	}


	return(
		<>
		<div className='centerReg'>
		<Form onSubmit={e => registerUser(e)}>
			<Form.Group>
				<Form.Label>First Name:</Form.Label>
				<Form.Control type="text" value={fName} onChange={e => {setFName(e.target.value)}} required/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Last Name:</Form.Label>
				<Form.Control type="text" value={lName} onChange={e => {setLName(e.target.value)}} required/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Email:</Form.Label>
				<Form.Control type="text" value={email} onChange={e => {setEmail(e.target.value)}} required/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Mobile Number:</Form.Label>
				<Form.Control type="number" value={mobileNo} onChange={e => {setMobileNo(e.target.value)}} required/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Password:</Form.Label>
				<Form.Control type="text" value={password} onChange={e => {setPassword(e.target.value)}} required/>
			</Form.Group>
			{
				isActive
				?<Button type="submit" className="btnMar">Register</Button>
				:<Button type="submit" disabled className="btnMar">Register</Button>
			}
		</Form>
		</div>
		</>
	)
}
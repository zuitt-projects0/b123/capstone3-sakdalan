import React,{useState,useEffect,useContext} from 'react';
import {Card,Button,Row,Col, Container} from 'react-bootstrap';
import {useParams,useHistory, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../userContext'
import '../srcCSS/universal.css';

export default function AddToCart(){
	const {prodId} = useParams();
	const {user} = useContext(UserContext);
	const history = useHistory();

	const [prodDetails,setProdDetails] = useState({
		image: null,
		name: null,
		desc: null,
		price: null,
		isActive: null

	})
	let token = localStorage.getItem('token')
	useEffect(()=>{
	
		fetch(`http://localhost:4000/products/getSingleProductNon/${prodId}`,{
			method: 'GET',
			headers:{
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			localStorage.getItem('token',data.accessToken);
			console.log(data);
			if(data.message){

				Swal.fire({

					icon: "error",
					title: "Course Unavailable",
					text: data.message

				})

			} else {

				console.log("passed")
				setProdDetails({
					image: data.image,
					name: data.name,
					desc: data.desc,
					price: data.price,
					isActive: data.isActive
				})
				
			}
		})

	},[prodId])

/*	const cart = (prodId) => {
		let token = localStorage.getItem('token')
		
		fetch('http://localhost:4000/orders/createOrder',{

			method: 'POST',
			headers: {
				"Content-Type": "application/json",
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				buyerId:localStorage.getItem('token'),
				totalAmount: prodDetails.price
			})
		})	
		.then(res => res.json())
		.then(data => {
			console.log(data)
		})
	}
*/
	const cart=()=>{
		Swal.fire({
			icon: "success",
			title: "Added to Cart",

		})
		history.push('/viewAll')
	}

	return (
		<Container>
					<Card className="mt-3 mb-2 viCard" >
						<Card.Body className="text-center">
							<Card.Img variant="top" className="statusImg "src={prodDetails.image} />
							<Card.Title>{prodDetails.name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{prodDetails.desc}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>{prodDetails.price}</Card.Text>
							<Button variant="primary" block onClick={()=> cart()}>Confirm Add To Cart</Button>
						</Card.Body>
					</Card>
		</Container>
		)



}
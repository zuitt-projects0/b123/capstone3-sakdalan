import React,{useState,useEffect,useContext} from 'react';
import {Card,Button,Row,Col, Container} from 'react-bootstrap';
import {useParams,useHistory, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../userContext'
import '../srcCSS/universal.css';

export default function OnOffProduct(){
	const {prodId} = useParams();
	const {user} = useContext(UserContext);
	const history = useHistory();

	const [prodDetails,setProdDetails] = useState({
		image: null,
		name: null,
		desc: null,
		price: null,
		isActive: null

	})
	let token = localStorage.getItem('token')
	useEffect(()=>{
	
		fetch(`http://localhost:4000/products/getSingleProduct/${prodId}`,{
			method: 'GET',
			headers:{
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			localStorage.getItem('token',data.accessToken);
			console.log(data);
			if(data.message){

				Swal.fire({

					icon: "error",
					title: "Course Unavailable",
					text: data.message

				})

			} else {

				console.log("passed")
				setProdDetails({
					image: data.image,
					name: data.name,
					desc: data.desc,
					price: data.price,
					isActive: data.isActive
				})
				
			}
		})

	},[prodId])

	const activate = (prodId) => {
		
		fetch(`http://localhost:4000/products/activateProduct/${prodId}`,{

			method: 'PUT',
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				prodId:prodId
			})
		})	
		.then(res => res.json())
		.then(data => {
			localStorage.getItem('token',data.accessToken);
			setProdDetails({
				image: data.image,
				name: data.name,
				desc: data.desc,
				price: data.price,
				isActive: data.isActive
			})
			console.log(data)
		})
	}

	const deactivate = (prodId) => {
		fetch(`http://localhost:4000/products/deactivateProduct/${prodId}`,{
			method: 'PUT',
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				prodId:prodId
			})
		})	
		.then(res => res.json())
		.then(data => {
			localStorage.getItem('token',data.accessToken);
			setProdDetails({
				image: data.image,
				name: data.name,
				desc: data.desc,
				price: data.price,
				isActive: data.isActive
			})
			console.log(data)
		})
	}

	return (
		<Container>
					<Card className="mt-3 mb-2 viCard" >
						<Card.Body className="text-center">
							<Card.Img variant="top" className="statusImg "src={prodDetails.image} />
							<Card.Title>{prodDetails.name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{prodDetails.desc}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>{prodDetails.price}</Card.Text>
							{
								prodDetails.isActive
								?
								<Button variant="primary" block onClick={()=> deactivate(prodId)}>Deactivate</Button>
								:
								<Button variant="primary"  className="m-2" block onClick={()=> activate(prodId)}>Activate</Button>
							}
							<Button variant="primary" className="m-2" as={Link} to={`/update/${prodId}`}>Update</Button>
							
						</Card.Body>
					</Card>
		</Container>
		)



}
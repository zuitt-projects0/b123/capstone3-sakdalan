import React,{useState,useEffect} from 'react';
import Product from '../components/Product'

export default function ViewProduct(){

	const [prodDetails,setProductDetails] = useState([])

	useEffect(()=>{
		fetch(`http://localhost:4000/products/retrieveAllActive`)
		.then(res => res.json())
		.then(data => {
			localStorage.getItem('token',data.accessToken);
			setProductDetails(data.map(product => {
				return (
					<Product prodProp={product}/>
				)
			}))	
		})
		.catch(err =>{
			console.log(err)
			alert('error')
		})
	},[])

	let rand = prodDetails[Math.floor(Math.random()*prodDetails.length)]
	return (
		<>
			<h1 className="my-5 text-center">Available Products</h1>
			{prodDetails}
		</>
		)
}
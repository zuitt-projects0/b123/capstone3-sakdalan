import React,{useState,useEffect} from 'react';
import 'bootswatch/dist/slate/bootstrap.min.css';
import './App.css';
import NavBar from './components/Navbar';
import Home from './Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import ViewProduct from './pages/ViewProducts';
import AddProduct from './pages/AddProduct';
import AllProduct from './pages/AllProduct';
import OnOffProduct from './pages/OnOffProduct';
import UpdateProduct from './pages/UpdateProduct';
import AddToCart from './pages/AddToCart';
import Footer from './components/Footer';
import {UserProvider} from './userContext'
import { BrowserRouter as Router } from 'react-router-dom';
import {Route,Switch} from 'react-router-dom';
import {Container} from 'react-bootstrap';
import './srcCSS/universal.css';

export default function App() {
  const [user,setUser] = useState({
    id: null,
    isAdmin: null
  })

  useEffect(()=>{

    fetch('http://localhost:4000/users/getUserDetails',{

      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }

    })
    .then(res => res.json())
    .then(data => {

      setUser({

        id: data._id,
        isAdmin: data.isAdmin

      })

    })

  },[])

  const unsetUser = () => {
    localStorage.clear()
  }

  return (
      <UserProvider value={{user,setUser,unsetUser}}>
        <Router>
          <NavBar/>
            <Switch>
              <Route exact path="/" component={Home}/>
              <Route exact path="/register" component= {Register}/>
              <Route exact path="/login" component= {Login}/>
              <Route exact path="/logout" component= {Logout}/>
              <Route exact path="/viewAll" component= {ViewProduct}/>
              <Route exact path="/addProduct" component= {AddProduct}/>
              <Route exact path="/viewAdmin" component= {AllProduct}/>
              <Route exact path="/cart/:prodId" component= {AddToCart}/>
              <Route exact path="/viewAdmin/:prodId" component= {OnOffProduct}/>
              <Route exact path="/update/:prodId" component= {UpdateProduct}/>
            </Switch>
        </Router>
        <Footer/>
      </UserProvider>
  );
}
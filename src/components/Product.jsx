import React,{useState,useEffect,useContext} from 'react';
import {CardGroup, Card, Button, Row, Col, Container} from 'react-bootstrap';
import {Link, useHistory} from 'react-router-dom';
import '../srcCSS/universal.css';
import UserContext from '../userContext'


export default function Product({prodProp}){
	const {user} = useContext(UserContext);
	const history = useHistory();


	return (
	<Container>
		<Card className="p-1 m-2 minCard text-center">
			<Card.Img variant="top" style={{ height: '20rem'}} src={prodProp.image} />
		    <Card.Body>
		    	<Card.Title style={{ height: '3rem'}}>{prodProp.name}</Card.Title>
		    	<Card.Text style={{ height: '5rem'}}>{prodProp.desc}</Card.Text>
		   	 	<Card.Text>PHP: {prodProp.price }</Card.Text>
		   	 	{
		   	 		user === null || user.isAdmin === false
		   	 		?
		   	 		<Link className="btn btn-secondary" to={`/cart/${prodProp._id}`}>Add To Cart</Link>	
		   	 		:
		   	 		<Link className="btn btn-danger btn-block" to="/login">Login to Shop</Link>
		   	 	}
			</Card.Body>
		</Card>
	</Container>
	)
}
